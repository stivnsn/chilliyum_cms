<?php 

require FileStructure::GetModel("ArticlesModel");
require_once FileStructure::GetModel("MainMenusModel");
interface ISingleArticle{
    function Index();
    function Show($articleName);
    function Insert($ignore);
    function Edit($articleName);
    function InsertMe($ignore);
    function Change($articleName);
}

class SingleArticle implements ISingleArticle {
    private $returnUrl;
    private $article;
    private $anyArticles;
    private $viewName;
    private $pseudoModuleName;
    private $userData;
    private $errorLog;
    private $categories;
    private $mainMenus;

    function __construct($userData) {
        // get action name, module and pseudo module name+ form return url if needed
        $this->anyArticles = true;
        $this->userData = $userData;
        $this->pseudoModuleName = Controller::$routingMech->pseudoModuleName;
        $this->categories = new CategoriesModel();
        $this->categories->getCategories();
        $this->mainMenus = new MainMenus();
        $this->mainMenus->getMainMenus();
    }

    function Index(){
        // shows article in group from provided pseudo controller
        
        $this->article = new ArticleModel();
        $this->anyArticles = $this->article->selectByMenuName($this->pseudoModuleName);
        $this->SetView("ShowArticle");
    }
    
    function Show($articleName) {
        // selects article by unique name and sets model
        $this->article = new ArticleModel();
        $this->article->selectByUniqueTitle($articleName);
        $this->SetView("ShowArticle");
    }

    function Insert($ignore) {
        // cleanes model for create form
        $this->SetView("InsertArticle");
    }

    function Edit($articleName) {
        // sets model for edit form
        $this->article = new ArticleModel();
        $this->article->selectByUniqueTitle($articleName);
        $this->SetView("EditArticle");
    }

    function Change($params)
    {
        $this->article = new ArticleModel();
        $articleSelected = $_POST["article_selected"];
        if ($this->checkPost("article_deletion"))
        {
            $this->article->deleteFromDb($articleSelected);
            $this->exitPost("article_deleted");
            return true;
        }
        else if ($this->checkPost("article_update"))
        {
            $this->article->create(
                $_POST["article_title"],
                $_POST["article_content"],
                $_POST["article_category"],
            1);
            $this->article->updateInDb($articleSelected);
            $this->article->selectByUniqueTitle($this->article->getUniqueTitle());
            $this->updateSAMenus();
            $this->exitPost("article_updated", $this->article->getUniqueTitle());
            return true;
        }
        return false;
    }

    function updateSAMenus(){
        foreach ($this->mainMenus->mainMenus as $mainMenu){
            if (!isset($_POST["article_mm_$mainMenu->id"])){
                continue;
            }
            $res = $this->mainMenus->updateMMDefaultArticle(
                $mainMenu->id, $this->article->id);
        }      
    }


    function InsertMe($params)
    {
        if (!$this->checkPost("article_insertion"))
        {
            return false;
        }
        $this->article = new ArticleModel();
        $this->article->create($_POST["article_title"], $_POST["article_content"], $_POST["article_category"], 1);
        $this->article->insertToDb();
        $this->updateSAMenus();
        $this->exitPost("article_inserted", $uniquTitle);
        return false;
    }
    
    private function checkPost($postName)
    {
        return isset($_POST[$postName]);
    }

    private function exitPost($sessName, $uniqueTitle)
    {
        $_SESSION[$sessName] = true;
        if ($uniqueTitle == NULL){
            header('Location: /' . $this->pseudoModuleName, true, 303);
        }
        else{
            header('Location: /Article/'.$uniqueTitle, true, 303);
        }
        exit();
    }


    private function drawModule()
    {
        require FileStructure::GetView($this->viewName, "SingleArticle");
    }
    private function SetView($viewName)
    {
        $this->viewName = $viewName;
        require FileStructure::GetView("Layout", "SingleArticle");
    }
    private function getErrorLog()
    {
        return $this->errorLog;
    }
}
?>
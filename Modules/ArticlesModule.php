<?php 
require FileStructure::GetModel("ArticlesModel");
interface IArticles
{
    public function Index();
//    public function Article($params);
}
class Articles implements IArticles
{
    private $user_data;
    private $errorLog;
    private $articles;
    private $categories;
    
    public function __construct($user_data)
    {
        $this->articles = new ArticlesModel();
        $this->articles->selectArticlesByMainMenu(Controller::$routingMech->pseudoModuleName);
        $this->categories = new CategoriesModel();
        $this->categories->getCategories();
        $this->user_data = $user_data;
    }
    function Index()
    {
        require FileStructure::GetView("Layout", "Articles");
    }
    
    private function drawModule()
    {
        require FileStructure::getView("Layout", "Articles");
    }

    function selectArticles(&$articles)
    {
        $pdoDriver = new PDODriver();
        $result = $pdoDriver->selectArticles($articles);
    	if (!$result)
    	{
            $this->errorLog .= $pdoDriver->getErrorLog();
            return false;
    	}
        return true;
    }

}
?>
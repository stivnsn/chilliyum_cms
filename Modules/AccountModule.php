<?php
interface IAccount
{
    public function Login();
    public function Register();
    public function Settings();
}
class AccountModule implements IAccount
{
    private $user_data;

    public function __construct()
    {
        
    }
    
    function Index()
    {
        $this->Login();
    }
    function Logout()
    {
        unset($_SESSION["user_data"]);
        header("Location: /Home", true, 303);
    }
    function Login()
    {
        require FileStructure::GetView("Login", "Account");
    }
    
    function Logme()
    {
        $this->getLoginPost($username, $password);
        $userData = new UserData();
        if (!$userData->login_user($username, $password))
        {
            echo ($userData->getErrorLog());
            exit();
            header("Location: /Account/Login", true, 303);
            exit();
        }
        $_SESSION["user_data"] = serialize($userData);
        header("Location: /Home", true, 303);
        exit();
    }

    function Register()
    {
        require FileStructure::GetView("Register", "Account");
    }
    
    function Regme()
    {
        $userData = new UserData();
        $this->getRegisterPost($userData);
        $userData->insertUser();
        header("Location: /Account/Login", true, 303);
        exit();
    }
        
    private function getRegisterPost(&$userData)
    {
        if(isset($_POST["reg_name"]))
        {
            $userData->name = $_POST["reg_name"];
        }
        if(isset($_POST["reg_surname"]))
        {
            $userData->surname = $_POST["reg_surname"];
        }
        if(isset($_POST["reg_username"]))
        {
            $userData->username = $_POST["reg_username"];
        }
        if(isset($_POST["reg_password"]))
        {
            $userData->password = md5($_POST["reg_password"]);
        }
        return;
    }

    private function getLoginPost(&$username, &$password)
    {
        if(isset($_POST["log_username"]))
        {
            $username = $_POST["log_username"];
        }
        if(isset($_POST["log_password"]))
        {
            $password = md5($_POST["log_password"]);
        }
        return;
    }

    function Settings()
    {
        require FileStructure::GetView("Settings", "Account");
    }
}
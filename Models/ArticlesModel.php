<?php
require_once FileStructure::GetDataMngmnt("ArticlesImpl");
class ArticlesModel
{
    public $articles= array();
    
    function __construct() {
        $this->dbDriver = new ArticlesDbImpl();
    }
    
    function __destruct() {
    }

    function selectArticles(){
        $this->dbDriver->selectArticles($rows);
        foreach($rows as $row) {
            $article = new Article();
            $article->fillWithDBRow($row);
            array_push($this->articles, $article);
        }
    }

    function selectArticlesByMainMenu($uniqueTitle)
    {
        $this->dbDriver->selectArticlesByMainMenu($rows, $uniqueTitle);
        foreach($rows as $row) {
            $article = new ArticleModel();
            $article->fillWithDBRow($row);
            array_push($this->articles, $article);
        }
        
    }

    function deleteArticle(&$article) {
        $article->deleteFromDb();
        // then refresh this object
    }

    function insertArticle(&$article) {
        $article->inserteToDb($article->uniqueTitle, $article->title, $article->content);
        //then refresh this object
    }

    function insertArticleByMenuName(&$article) {
        $this->dbDriver->insertToDbByMenuName($article->uniqueTitle, $article->title, $article->content);
        // then refresh this object
    }

    function updateArticle(&$article) {
        $article->updateArticleInDb();
        // then refresh this object
    }
    
}

class ArticleModel
{
    public $id;
	public $uniqueTitle;
	public $title;
	public $content;
	public $position;
    public $category;
    private $errorLog;
    
	function __construct ()
	{
        $this->dbDriver = new ArticlesDbImpl();
	}

    function create($title, $content, $category, $position)
    {
        $this->clear();
        $this->id = NULL;
        $this->uniqueTitle = NULL;
        $this->title = $title;
        $this->content = $content;
        $this->position = $position;
        $this->category = $category;
    }

    function checkIfValid(){
        if (sizeof($this->title) <= 0){
            return false;
        }
        if (sizeof($this->content) <= 0){
            return false;
        }
        return true;
    }
    
    function clear(){
        $this->id = NULL;
        $this->title = NULL;
        $this->content = NULL;
        $this->uniqueTitle = NULL;
        $this->position = NULL;
        $this->category = NULL;
    }

    function fillWithDBRow($db_row)
    {
        $this->id = $db_row['a_ID'];
		$this->title = $db_row['a_Title'];
		$this->content = $db_row['a_Content'];
		$this->uniqueTitle = $db_row['a_UniqueTitle'];
		$this->position = $db_row['a_Position'];
        $this->category = new CategoryModel();
        $this->category->fillWithDbRow($db_row);
    }

    function getUniqueTitle()
    {
        return $this->uniqueTitle;
    }
    
    function selectByMenuName($uniqueTitle)
    {
        $retval = $this->dbDriver->selectDefaultMenuArticle($this, $uniqueTitle);
        return $retval;
    }

    function insertToDb(){
        $this->dbDriver->insertArticle($this->uniqueTitle, $this->title, $this->content, $this->category);
        $this->dbDriver->selectByUniqueTitle($this, $this->uniqueTitle);
    }

    function insertToDbByMenuName()
    {
        $this->dbDriver->insertArticleByMenuName($this->uniqueTitle, $this->title, $this->content);
    }

    function updateInDb($uniqueTitle){
        $this->uniqueTitle = $uniqueTitle;
        $this->dbDriver->updateArticle($this->uniqueTitle, $this->title, $this->content, $this->category) ;
    }

    function deleteFromDb($uniqueTitle){
        $this->dbDriver->deleteArticle($uniqueTitle);
        $this->clear();
    }

    function selectDefaultMenuArticle($uniqueTitle){
        $this->dbDriver->selectDefaultMenuArticle($this, $uniqueTitle);
    }

    function selectByUniqueTitle($uniqueTitle) {
        $this->dbDriver->selectByUniqueTitle($this, $uniqueTitle);
    }

}

class CategoriesModel {
    public $categories = array();
    function __construct ()
	{
        $this->dbDriver = new ArticlesDbImpl();
	}
    function getCategories() {
        $this->dbDriver->selectAllCategories($rows);
        $this->fillWithDbRows($rows);
        echo $this->dbDriver->getErrorLog();
    }
    private function fillWithDbRows(&$rows){
        foreach ($rows as $row) {
            $category = new CategoryModel();
            $category->fillWithDbRow($row);
            array_push($this->categories, $category);
        }
    }
}

class CategoryModel {
    public $id;
    public $title;
    public $uniqueTitle;
    public $description;
    function fillWithDbRow(&$row){
        $this->id = $row["ac_ID"];
        $this->title = $row["ac_Title"];
        $this->uniqueTitle = $row["ac_UniqueTitle"];
        $this->description = $row["ac_Description"];
    }
}
?>
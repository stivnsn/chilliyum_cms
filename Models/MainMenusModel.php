<?php
require_once FileStructure::GetDataMngmnt("SettingsImpl");
class MainMenu
{
	public $title;
	public $uniqueTitle;
	public $description;
	public $pseudoModuleName;
	public $position;
    public $defaultArticleId;
    
    function __construct ()
	{
	}

    function fillWithDbRow($db_row)
    {
        $this->id = $db_row['mm_ID'];
		$this->title = $db_row['mm_Title'];
		$this->description = $db_row['mm_Description'];
		$this->uniqueTitle = $db_row['mm_UniqueTitle'];
		$this->pseudoModuleName = $db_row['pmn_PseudoTitleUnique'];
		$this->position = $db_row['mm_Position'];
        $this->defaultArticlesId = $db_row['mm_DefaultArticlesID'];
    }
}

class MainMenus
{
    public $mainMenus = array();
    private $dbModel;
    function __construct (){
        $this->dbModel = new SettingsDbImpl();
    }
    
    function getMainMenus()
    {
        $this->dbModel->selectMainMenus($this->mainMenus);
    }

    function updateMMDefaultArticle($mmIds, $articleId){
        $this->dbModel->updateMMDefaultArticle($mmIds, $articleId);
        $this->dbModel->getErrorLog();
    }
}
?>
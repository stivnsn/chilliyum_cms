<?php
require_once Filestructure::GetDataMngmnt("AccountImpl");
class UserData
{
    const ROLE_SUPERADMIN = 1;
	const ROLE_ADMIN = 2;
    const ROLE_EDITOR = 3;
	const ROLE_USER = 4;
	const ROLE_ANON = 5;
    public $username;
	public $role;
	public $logged_in;
    public $name;
    public $surname;
    public $password;
    private $errorLog;
    private $dbDriver;
	function __construct()
	{
        $this->role = 5;
		$this->logged_in = false;
	}   
    function clear_user()
    {
        $this->username = NULL;
        $this->password = NULL;
        $this->name = NULL;
        $this->surname = NULL;
        $this->role = 5;
        $this->logged_in = false;
    }
	function check_user($username, $password)
	{
        $dbDriver = new AccountDbImpl();
        $retval = $dbDriver->checkUser($username, $password);
        return $retval;
	}
    function login_user($username, $password)
    {
        $dbDriver = new AccountDbImpl();
        $dbDriver->getUser($username, $password, $this);
        return $this->logged_in;
    }
	function logout_user()
	{
	    $this->clear_user();
	}
	function is_admin()
	{
		return $this->role == self::ROLE_ADMIN && $this->is_logged_in();
	}
	function is_logged_in()
	{
		return $this->logged_in;
	}

    function insertUserToDb()
    {
        $dbDriver = new AccountDbImpl();
        $dbDriver->insertUser($this->name, $this->surname, 
        $this->username, 
        $this->password);
    }
    
    function getErrorLog()
    {
        return $this->errorLog;
    }
}
?>
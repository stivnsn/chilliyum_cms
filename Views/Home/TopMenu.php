<?php
if (AuthenticationProvider::isAdminLogged($this->user_data))
{
    echo 'Hi, '. $this->user_data->name .'! <a href="/Account/Logout">Logout</a>';
}
else
{
    echo '<a href="/Account/Login" >Login</a> <a href="/Account/Register">Register</a>';
}
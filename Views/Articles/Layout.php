<?php 
if (isset($_SESSION["article_inserted"]))
{
?>
Article Updated: new one should be listed.
<?php		
unset($_SESSION["article_inserted"]);
}
?>
<div id="articles">
  <?php
for ($i = 0; $i < sizeof($this->articles->articles); ++$i)
{
?>
		<div class="article">
			<h2>
<?php
echo $this->articles->articles[$i]->title;
?>
</h2>
<?php
if (AuthenticationProvider::isAdminLogged($this->user_data))
{
	?>
         <a href="/Article/Edit/<?php echo$this->articles->articles[$i]->uniqueTitle; ?>">Edit</a>
	<form action="/Article/Change/editArticle" method="POST">
	      <input type="hidden" name="article_selected" value="<?php echo $this->articles->articles[$i]->uniqueTitle; ?>"/>
	      <input type="submit" name="article_deletion" value="Delete" />
	</form>
	<?php
}
?>
<p>
<?php echo $this->articles->articles[$i]->content;?><a href="/Article/<?php echo $this->articles->articles[$i]->uniqueTitle;?>">More</a>
</p>
		</div>
<?php 
}
if (AuthenticationProvider::isAdminLogged($this->user_data))
{
    FileStructure::GetView("InsertArticle", "SingleArticle");
}
?>

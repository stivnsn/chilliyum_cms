<!DOCTYPE html> 
<html>
  <head>
    <title>ChilliYUM!</title>
    <link rel="stylesheet" href="<?php echo FileStructure::GetStyle("main.css"); ?>" />
  </head>
  <body>
    <div id="top_bar">
      <?php 
         $this->drawTopMenu();
      ?>
    </div>
	  <div id="page_limit">
	    <div id="header">
		  <img style="width: 300px" 
			   src="<?php echo FileStructure::GetImage("chilliyum_logo_large.png"); ?>" />
	    </div>
        <?php
	       $this->drawController();
        ?>
	  </div>
  </body>
</html>

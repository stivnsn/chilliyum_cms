<?php
if($this->userData->is_admin()) {
?>
<form action="/Article/InsertMe/NewArticle" method="POST">
     <label>
     <span>Title</span>
     <input type="text" id="article_title_input" name="article_title" />
     </label>
     <label>
     <span>Content</span>
    <textarea name="article_content" id="article_content"></textarea>

    <script>CKEDITOR.replace( 'article_content' );</script>
     </label>
    <label>
	 <?php
	    foreach ($this->mainMenus->mainMenus as $key => $mainMenu)
         {
	 ?>
	    <input type="checkbox" name="article_mm_<?php echo $mainMenu->id; ?>"
		   <?php
		      echo ($mainMenu->defaultArticlesId == $this->article->id ? "checked" : NULL );
		      ?>/><?php echo $mainMenu->title;

	    }?>
        </label>
     <label>
       <select id="categories" name="article_category">	  
	 <?php
	    foreach ($this->categories->categories as $category)
	 {
	 ?>
	    <option value="<?php echo $category->id;?>"><?php echo $category->title;?></option>
<?php
   }
   ?>
</select>  
</label>
<input type="submit" name="article_insertion" value="Submit" />
</form>
<?php
}
?>

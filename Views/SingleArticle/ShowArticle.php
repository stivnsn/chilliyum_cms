<div id="single_article_container">
<?php
     if( !$this->anyArticles)
         {
             if($this->userData->is_admin()){
                 require FileStructure::GetView("InsertArticle", "SingleArticle");
             }
         }
     else
         {
?>
     <h1><?php echo $this->article->title; ?></h1>
<?php
             if($this->userData->is_admin()){
?>
                 <a href="/Article/Edit/<?php echo $this->article->uniqueTitle; ?>">Edit</a>
                 <form action="/Article/Change/EditArticle" method="POST">
                 <input type="hidden" name="article_selected" value="<?php echo $this->article->uniqueTitle;?>" />
                 <input type="submit" value="Delete" name="article_deletion"/>
                 </form>
<?php
             }
?>
             <p><?php echo $this->article->content; ?></p>
<?php
}
?>
</div>

<?php
if ($this->userData->is_admin()){
?>
    <script src="/ckeditor/ckeditor.js"></script>
<form action="/Article/Change/<?php echo $this->article->uniqueTitle;?>" method="POST">
     <label>
     <span>Title</span>
     <input type="hidden" name="article_selected" value="<?php echo $this->article->uniqueTitle; ?>"/>
     <input type="hidden" name="article_id" value="<?php echo $this->article->id; ?>" />
     <input type="text" id="article_title_input" name="article_title" 
     value="<?php echo $this->article->title; ?>" />
     </label>
     <label>
	 <?php
	    foreach ($this->mainMenus->mainMenus as $key => $mainMenu)
         {
	 ?>
     <input type="checkbox" name="article_mm_<?php echo $mainMenu->id; ?>"
		   <?php
		      echo ($mainMenu->defaultArticlesId == $this->article->id ? "checked" : NULL );
		      ?>/><?php echo $mainMenu->title;

	    }?></label>
     <label>
       <select id="categories" name="article_category">	  
	 <?php
	    foreach ($this->categories->categories as $category)
         {
	 ?>
	    <option value="<?php echo $category->id;?>"><?php echo $category->title;?></option>
	    <?php
	       }
	       ?>
       </select>  
     </label>
     <label>
       <span>Content</span>
     </label>
       <textarea id="article_content" name="article_content"><?php echo $this->article->content; ?></textarea>
       <script>CKEDITOR.replace( 'article_content' );</script>
     <input type="submit" name="article_update" value="Update" />
     <input type="submit" name="article_deletion" value="Delete" />
</form>
<?php
}
?>

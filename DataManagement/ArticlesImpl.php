<?php
require_once FileStructure::GetDataMngmnt("PDODriver");
class ArticlesDbImpl extends PDODriver{
    private $articlesTbl = "a.ID AS a_ID, a.Title AS a_Title, a.UniqueTitle AS a_UniqueTitle," .
        " a.Position AS a_Position, a.Content AS a_Content";
    private $categoriesTbl = "ac.ID AS ac_ID, ac.UniqueTitle AS ac_UniqueTitle, ac.Title AS ac_Title," .
                " ac.Description AS ac_Description";
    
    function __construct(){
        parent::__construct();
    }
    
    function getUniqueTitle($articleTitle){
        $articleTitle = str_replace(" ", "-", $articleTitle);
        $articleTitle = preg_replace("/[^a-z0-9\s\-]/i", "", $articleTitle);
        $articleTitle = substr($articleTitle, 0, 50);
        $articleTitle .= "-" . time();
        return $articleTitle;
    }
    
    function deleteArticle($uniqueTitle) {
        try {   
            if ($this->instance == NULL){
                throw (new PDOException("DB Driver failed."));
            }
            $stmt = $this->instance->prepare("DELETE FROM Articles" .
            " WHERE UniqueTitle = :UniqueTitle;");
            $stmt->bindParam(":UniqueTitle", $uniqueTitle, PDO::PARAM_STR);
            if (!$stmt->execute()){
                throw (new PDOException($stmt->errorCode()));
            }
        }
        catch(PDOException $ex){
            $this->errorLog .= "Error: " .$ex->getMessage() . "\r\n";
            return false;
        }
        catch(Exception $ex){
            $this->errorLog .= "Error: " . $ex->getMessage() . "\r\n";
            return false;
        }
        return true;
    }
    
    function insertArticle(&$uniqueTitle, $articleTitle, $articleContent, $categoryId){
        try {
            $uniqueTitle = $this->getUniqueTitle($articleTitle);	
            if ($this->instance == NULL){
                throw (new PDOException("DB Driver failed."));
            }
            $stmt = $this->instance->prepare("INSERT INTO Articles" .
            " (UniqueTitle, Title, Content, ArticleCategoryID) VALUES" .
            " (:UniqueTitle, :Title, :Content, :CategoryID);");
            $stmt->bindParam(":UniqueTitle", $uniqueTitle, PDO::PARAM_STR);
            $stmt->bindParam(":Title", $articleTitle, PDO::PARAM_STR);
            $stmt->bindParam(":Content", $articleContent, PDO::PARAM_STR);
            $stmt->bindParam(":CategoryID", $categoryId, PDO::PARAM_STR);
            if (!$stmt->execute()){
                throw (new PDOException($stmt->errorCode()));
            }
        }
        catch(PDOException $ex){
            $this->errorLog .= "Error: " .$ex->getMessage() . "\r\n";
            return false;
        }
        catch(Exception $ex){
            $this->errorLog .= "Error: " . $ex->getMessage() . "\r\n";
            return false;
        }
        return true;
    }
    
    function insertArticleByMenuName(&$uniqueTitle, $articleTitle, 
    $articleContent, $menuName){
        try
            {
                $uniqueTitle = $this->getUniqueTitle($articleTitle);	
                if ($this->instance == NULL){
                    throw (new PDOException("DB Driver failed."));
                }
                $stmt = $this->instance->prepare("INSERT INTO Articles" .
                " (UniqueTitle, Title, Content) VALUES" .
                " (:UniqueTitle, :Title, :Content);");
                $stmt->bindParam(":UniqueTitle", $uniqueTitle, PDO::PARAM_STR);
                $stmt->bindParam(":Title", $articleTitle, PDO::PARAM_STR);
                $stmt->bindParam(":Content", $articleContent, PDO::PARAM_STR);
                
                if (!$stmt->execute()){
                    throw (new PDOException($stmt->errorCode()));
                }
            }
        catch(PDOException $ex){
            $this->errorLog .= "Error: " .$ex->getMessage() . "\r\n";
            return false;
        }
        catch(Exception $ex){
            $this->errorLog .= "Error: " . $ex->getMessage() . "\r\n";
            return false;
        }
        return true;
    }
    
    function updateArticle(&$uniqueTitle, $articleTitle, $articleContent, $categoryId)	{
        try{
            $oldUniqueTitle = $uniqueTitle;
            $uniqueTitle = $this->getUniqueTitle($articleTitle);	
            if ($this->instance == NULL){
                throw (new PDOException("DB Driver failed."));
            }
            urlencode($articleContent);
            $stmt = $this->instance->prepare("UPDATE Articles SET" .
            " UniqueTitle = :UniqueTitle, Title = :Title,".
            " Content=:Content, ArticleCategoryID = :CategoryID WHERE UniqueTitle=:OldUniqueTitle ;");
            $stmt->bindParam(":UniqueTitle", $uniqueTitle, PDO::PARAM_STR);
            $stmt->bindParam(":Title", $articleTitle, PDO::PARAM_STR);
            $stmt->bindParam(":Content", $articleContent, PDO::PARAM_STR);
            $stmt->bindParam(":OldUniqueTitle", $oldUniqueTitle, PDO::PARAM_STR);
            $stmt->bindParam(":CategoryID", $categoryId, PDO::PARAM_STR);
            if (!$stmt->execute()){
                throw (new PDOException());
            }
        }
        catch(PDOException $ex){
            $this->errorLog .= "Error: " .$ex->getMessage() . "\r\n";
            return false;
        }
        catch(Exception $ex){
            $this->errorLog .= "Error: " . $ex->getMessage() . "\r\n";
            return false;
        }
        return true;
    }
    
    
    function selectArticles(&$articles){
        try {
            if ($this->instance == NULL){
                throw (new PDOException("DB Driver failed."));
            }
            $stmt = $this->instance->prepare(
                "SELECT $this->articlesTbl, $this->categoriesTbl FROM Articles a " .
                " INNER JOIN ArticleCategories ac" .
                " ON ac.ID = a.ArticleCategoriesID ORDER BY a_Position;"
            );	
            if (!$stmt->execute()) {
                throw (new PDOException($stmt->errorCode()));
            }
            $rows = $stmt->fetchAll();
            $stmt->closeCursor();
        }
        catch (PDOException $e){
            $this->errorLog .= "Error: " . $e->getMessage() . "\r\n";
            return false;
        }
        catch (Exception $ex){
            $this->errorLog .= "Error: " . $e-> getMessage() . "\r\n";
            return false;
        }
        return true;
    }
    
    function selectArticlesByMainMenu(&$rows, $uniqueTitle){
        try {
            if ($this->instance == NULL){
                throw (new PDOException("DB Driver failed."));
            }
            $stmt = $this->instance->prepare(
                "SELECT $this->articlesTbl, $this->categoriesTbl" .
                " FROM Articles a INNER JOIN ArticleCategories ac" .
                " ON a.ArticleCategoryID = ac.ID" .
                " INNER JOIN MainMenuArticleCategories mmac" .
                " ON ac.ID = mmac.ArticleCategoriesID " .
                " INNER JOIN MainMenus mm ON mm.ID = mmac.MainMenusID" .
                " WHERE mm.UniqueTitle = :UniqueTitle ORDER BY a.Position;"
            );	
            $stmt->bindParam(":UniqueTitle", $uniqueTitle, PDO::PARAM_STR);
            if (!$stmt->execute()) {
                throw (new PDOException($stmt->errorCode()));
            }
            $rows = $stmt->fetchAll();
            $stmt->closeCursor();
        }
        catch (PDOException $e){
            $this->errorLog .= "Error: " . $e->getMessage() . "\r\n";
            return false;
        }
        catch (Exception $ex){
            $this->errorLog .= "Error: " . $e-> getMessage() . "\r\n";
            return false;
        }
        return true;
    }
    
    function selectDefaultMenuArticle(&$article, $uniqueTitle) {
        try {
            if ($this->instance == NULL){
                throw (new PDOException("DB Driver failed."));
            }
            $stmt = $this->instance->prepare(
                "SELECT $this->articlesTbl, $this->categoriesTbl" .
                " FROM Articles a INNER JOIN MainMenus mm ON mm.DefaultArticlesID = a.ID" .
                " INNER JOIN ArticleCategories ac ON a.ArticleCategoryID = ac.ID " .
                " WHERE mm.UniqueTitle=:UniqueTitle;");
            $stmt->bindParam(":UniqueTitle", $uniqueTitle, PDO::PARAM_STR);
            
            if (!$stmt->execute()){
                throw (new PDOException($stmt->errorCode()));
            }
            if ($row = $stmt->fetch()){
                
                if ($row == NULL){
                    throw(new PDOException("Row empty: " . 
                    $row['Title'] . " " . $row['Content']));
                }
                $article->fillWithDBRow($row);
            }
            else {
                throw(new PDOException("No article by that unique name found"));
            }
            $stmt->closeCursor();
        }
        catch (PDOException $e){
            $this->errorLog .= "Error: " . $e->getMessage() . "\r\n";
            return false;
        }
        catch (Exception $ex){
            $this->errorLog .= "Error: " . $e-> getMessage() . "\r\n";
            return false;
        }
        return true;
    }
    
    
    function selectByUniqueTitle(&$article, $uniqueTitle) {
        try {
            if ($this->instance == NULL){
                throw (new PDOException("DB Driver failed."));
            }
            $stmt = $this->instance->prepare("SELECT $this->articlesTbl, $this->categoriesTbl" .
            " FROM Articles a INNER JOIN ArticleCategories ac ON ac.ID = a.ArticleCategoryID" .
            " WHERE a.UniqueTitle=:UniqueTitle;");	
            $stmt->bindParam(":UniqueTitle", $uniqueTitle, PDO::PARAM_STR);
            if (!$stmt->execute()){
                throw (new PDOException($stmt->errorCode()));
            }
            if ($row = $stmt->fetch()){
                if ($row == NULL){
                    throw(new PDOException("Row empty: " . 
                    $row['Title'] . " " . $row['Content']));
                }
                $article->fillWithDBRow($row);
            }
            else {
                throw(new PDOException("No article by that unique name found"));
            }
            $stmt->closeCursor();
        }
        catch (PDOException $e) {
            $this->errorLog .= "Error: " . $e->getMessage() . "\r\n";
            return false;
        }
        catch (Exception $ex){
            $this->errorLog .= "Error: " . $e-> getMessage() . "\r\n";
            return false;
        }
        return true;
    }
    
    function selectAllCategories(&$rows) {
        try {
            if ($this->instance == NULL) {
                throw (new PDOException("DB Driver failed. "));
            }
            $stmt = $this->instance->prepare("SELECT $this->categoriesTbl".
            " FROM ArticleCategories ac ORDER BY Title;");
            if (!$stmt->execute()){
                throw (new PDOException($stmt->errorCode()));
            }
            $rows = $stmt->fetchAll();            
            $stmt->closeCursor();
        }
        catch (PDOException $e) {
            $this->errorLog .= "Error: " . $e->getMessage() . "\r\n";
            return false;
        }
        catch (Exception $ex) {
            $this->errorLog .= "Error: " . $e->getMessage() . "\r\n";
            return false;
        }
        return true;        
    }
};

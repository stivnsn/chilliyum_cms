<?php
require_once FileStructure::GetDataMngmnt("PDODriver");
class SettingsDbImpl extends PDODriver{
    private $selectQuery = " mm.ID AS mm_ID, mm.Title AS mm_Title, mm.UniqueTitle AS mm_UniqueTitle," .
        " mm.Description AS mm_Description, pmn.PseudoTitleUnique AS pmn_PseudoTitleUnique, " .
        " mm.Position AS mm_Position, mm.DefaultArticlesID AS mm_DefaultArticlesID";
	function selectModuleByPseudo(&$moduleName, $pseudoModuleName)
	{
		try
		{
			if($this->instance == NULL)
			{
				throw (new PDOException("DB Driver failed."));
			}
			$stmt = $this->instance->prepare(
			"SELECT ModuleName FROM PseudoModuleNames".
			" WHERE PseudoTitleUnique = :PseudoTitleUnique;");
			$stmt->bindParam(":PseudoTitleUnique", $pseudoModuleName, PDO::PARAM_STR);
			if (!$stmt->execute())
			{
				throw (new PDOException("DB Driver failed. error " . $stmt->errorCode()));
			}
			$result = $stmt->fetch();
			if (!isset($result["ModuleName"]))
			{
				throw new Exception("No result fetched");
			}
			$moduleName = $result["ModuleName"];
		}
		catch (PDOException $e)
      		{
			$this->errorLog .= "Error: " . $e->getMessage() . "\r\n";
			return false;
		}
		catch (Exception $e)
		{
			$this->errorLog .= "Error: " . $e->getMessage() . "\r\n";
			return false;
		}
		return true;
	}

    public function updateMMDefaultArticle($mmId, $articleId){
        
		try
		{
			if ($this->instance == NULL)
			{
				throw new PDOException("Instance was not initialized");
			}
			$stmt = $this->instance->prepare("UPDATE MainMenus SET DefaultArticlesID = :DefaultArticlesID" .
            " WHERE ID = :mmID;");
            $stmt->bindValue(":DefaultArticlesID", 
            $articleId == NULL ? null : $articleId, PDO::PARAM_STR);
            $stmt->bindParam(":mmID", $mmId, PDO::PARAM_STR);
			if (!$stmt->execute())
			{
				throw (new PDOException($stmt->errorCode()));
			}
		}
		catch (PDOException $e)
      		{
			$this->errorLog .= "Error: " . $e->getMessage() . "\r\n";
			return false;
		}
		catch (Exception $ex)
		{
			$this->errorLog .= "Error: " . $e-> getMessage() . "\r\n";
			return false;
		}
		return true;
    }
			
	public function selectMainMenus(&$mainMenus)
	{
		try
		{
			if ($this->instance == NULL)
			{
				throw new PDOException("Instance was not initialized");
			}
			$stmt = $this->instance->prepare("SELECT $this->selectQuery FROM MainMenus mm INNER JOIN".
			      	" PseudoModuleNames pmn ON mm.PseudoModuleNamesID = pmn.ID ORDER BY mm.Position;");	
			if (!$stmt->execute())
			{
				throw (new PDOException($stmt->errorCode()));
			}
			while ($row = $stmt->fetch())
			{
				if ($row == NULL)
				{
					throw(new PDOException("Row empty: " . 
					$row['Title'] . " " . $row['Content']));
				}
				$mainMenu = new MainMenu();
                $mainMenu->fillWithDbRow($row);
				array_push($mainMenus, $mainMenu);
			}
			$stmt->closeCursor();
		}
		catch (PDOException $e)
      		{
			$this->errorLog .= "Error: " . $e->getMessage() . "\r\n";
			return false;
		}
		catch (Exception $ex)
		{
			$this->errorLog .= "Error: " . $e-> getMessage() . "\r\n";
			return false;
		}
		return true;
	}
}
?>
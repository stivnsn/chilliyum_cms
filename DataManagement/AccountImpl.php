<?php
require_once FileStructure::GetDataMngmnt("PDODriver");
class AccountDBImpl extends PDODriver {
  
  function __construct()
  {
    parent::__construct();
  }

  function getUser($username, $password, &$userData){
    try{
      $userData->clear_user();
      if ($this->instance == NULL){
	throw (new PDOException("DB Driver failed."));
      }
      $stmt = $this->instance->prepare("SELECT Username, " .
				       " FirstName, LastName, RoleID FROM Users WHERE Username = :Username AND" .
				       " Password = :Password;");
      $stmt->bindParam(":Username", $username, PDO::PARAM_STR);
      $stmt->bindParam(":Password", $password, PDO::PARAM_STR);
      if (!$stmt->execute()){
	throw (new PDOException($stmt->errorCode()));
      }
      if ($row = $stmt->fetch()) {
	if ($row == NULL) {
	  throw(new PDOException("Row empty! "));
	}
	$userData->username = $row["Username"];
	$userData->name = $row["FirstName"];
	$userData->surname = $row["LastName"];
	$userData->role = $row["RoleID"];
	$userData->logged_in = true;
	if ($row = $stmt->fetch()) {
	  $userData->clear_user();
	  throw(new PDOException("More than one username! Clearing user data."));
	}
      }
      else {
	$this->errorLog .= "No users found. \r\n";
      }
      $stmt->closeCursor();
    }
    catch(PDOException $ex){
      $this->errorLog .= "Error: " .$ex->getMessage() . "\r\n";
      return false;
    }
    catch(Exception $ex){
      $this->errorLog .= "Error: " . $ex->getMessage() . "\r\n";
      return false;
    }
    return $userData->logged_in;
  }

  function checkUser($username, $password) {
      $retval = false;
      try{
          if ($this->instance == NULL){
              throw (new PDOException("DB Driver failed."));
          }
          $stmt = $this->instance->prepare("SELECT COUNT(Username) as count".
          " FROM Users WHERE Username = :Username AND " .
          " Password = :Password;");
          $stmt->bindParam(":Username", $username, PDO::PARAM_STR);
          $stmt->bindParam(":Password", $password, PDO::PARAM_STR);
          if (!$stmt->execute()){
              throw (new PDOException($stmt->errorCode()));
          }
          $row = $stmt->fetch();
          $retval = $row["count"] == 1;
          $stmt->closeCursor();
      }
      catch(PDOException $ex){
          $this->errorLog .= "Error: " .$ex->getMessage() . "\r\n";
          return false;
      }
      catch(Exception $ex){
          $this->errorLog .= "Error: " . $ex->getMessage() . "\r\n";
          return false;
      }
      return $retval;
  }

  function insertUser($name, $surname, $username, $password) {
      try {
          if ($this->instance == NULL) {
	throw (new PDOException("DB Driver failed."));
          }
          $stmt = $this->instance->prepare("INSERT INTO Users" .
          " (FirstName, LastName, Username, Password, RoleID) VALUES" .
          " (:FirstName, :LastName, :Username, :Password, 4);");
          $stmt->bindParam(":FirstName", $name, PDO::PARAM_STR);
          $stmt->bindParam(":LastName", $surname, PDO::PARAM_STR);
          $stmt->bindParam(":Username", $username, PDO::PARAM_STR);			
          $stmt->bindParam(":Password", $password, PDO::PARAM_STR);
          //$stmt->bindParam(":RoleID", 4, PDO::PARAM_STR);
          if (!$stmt->execute())	{
              throw (new PDOException($stmt->errorCode()));
          }
      }
      catch(PDOException $ex){
          $this->errorLog .= "Error: " .$ex->getMessage() . "\r\n";
          return false;
    }
    catch(Exception $ex) {
      $this->errorLog .= "Error: " . $ex->getMessage() . "\r\n";
      return false;
    }
    return true;
  }
  };
?>

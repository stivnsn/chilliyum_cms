<?php
class HomeController extends Controller
{
	private $moduleInstance;
	public function __construct()
	{
		parent::__construct();
        $this->loadModule();
		$this->loadMainMenu();
        $this->draw();
	}
	public function drawController()
	{
		//here we require appropriate view with an action attribute;
		require FileStructure::GetView("Layout", "Home"); 
	}

	public function drawModule()
	{
		$actionName = self::$routingMech->actionName;
        $params = self::$routingMech->params;
        	if (!$this->moduleInstance || !$actionName)
        	{
         	   return;
        	}
		$this->moduleInstance->$actionName($params);
	}

	public function loadModule()
	{
		$moduleName = self::$routingMech->moduleName;
		if ($moduleName == NULL)
		{	
			return false;
		}
		require FileStructure::GetModule($moduleName);
		$this->moduleInstance = new $moduleName($this->user_data);
	}
	private $errorLog;
	function getErrorLog()
	{
		return $this->errorLog;
	}
}
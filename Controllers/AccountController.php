<?php
class AccountController extends Controller
{
    private $moduleInstance;
    public function __construct()
    {
        parent::__construct();
        $this->loadModule();
        $this->loadMainMenu();
        $this->draw();
    }

    public function drawController()
    {
        require FileStructure::GetView("Layout", "Account");
    }

    public function drawModule()
    {
        $actionName = self::$routingMech->actionName;
        $params = self::$routingMech->params;
        if(!$this->moduleInstance || !$actionName )
        {
            return;
        }
        if(!method_exists($this->moduleInstance, $actionName))
        {
            self::return404();
        }
        $this->moduleInstance->$actionName($params);
    }	
    public function loadModule()
	{
        $moduleName = self::$routingMech->controllerName;
		if ($moduleName == NULL)
        {
            return;
		}
		require FileStructure::GetModule($moduleName);
        $moduleName .= "Module";
        if (!class_exists($moduleName))
		{
            self::return404();
        }
		$this->moduleInstance = new $moduleName();
	}
}
?>
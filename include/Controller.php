<?php
if (!FileStructure::requireIfExists(
    FileStructure::GetInclude("AuthenticationProvider.php")))
    {
        Controller::return404();
    }

require FileStructure::GetModel("MainMenusModel");
abstract class Controller extends AuthenticationProvider
{
    public static $routingMech;
	public $mainMenus = array();
    private $errorLog;
    public static final function Start()
    {
        self::$routingMech = new RoutingMechanism();
        if (!FileStructure::requireIfExists(
            FileStructure::GetController(
                self::$routingMech->controllerName)))
        {
            self::return404();
        } 
        $controllerName = self::$routingMech->controllerFullName;
        if (!class_exists($controllerName))
        {
            self::return404();
        }
        new $controllerName();
    }
	public function __construct()
	{
        parent::__construct();
	}
	public final function draw()
	{
		require FileStructure::GetView("Layout");
	}

	protected abstract function drawController();

	public function loadMainMenu()
	{
		$this->mainMenus = new MainMenus();
        $this->mainMenus->getMainMenus();
	}

    public final function drawTopMenu()
    {
        require FileStructure::GetView("TopMenu", "Home");
    }

	public final function drawMainMenu()
	{
        require FileStructure::GetView("MainMenu", "Home");// generates mainMenu
	}

	public final function getProfileMenu()
	{
		//generates profile, login ...
	}

    public static final function return404()
    {
        header("ErrorDocument 404 /notFound.php");
        exit();
    }
}
?>
<?php 
class FileStructure
{
	const CONTROLLERS_DIR = "/Controllers";
	const VIEWS_DIR = "/Views";
	const MODELS_DIR = "/Models";
    const MODULES_DIR = "/Modules";
	const CONTENT_DIR = "/Content";
	const STYLES_DIR = "/Styles";
	const IMAGES_DIR = "/Images";
	const DATA_MNGMNT_DIR = "/DataManagement";
	const SERVER_MNGMNT_DIR = "/ServerManagement";
	const COMMON_DIR = "/CommonFunctions";
	const INCLUDE_DIR = "/include";
    
	private static function normalize($filename)
	{
		return "/" . trim($filename, '/');
	}

    static function requireIfExists($filename)
    {
        if (file_exists($filename))
        {
            require $filename;
            return true;
        }
        return false;
    }

	static function GetInclude($filename)
	{
		return __ROOT_PATH . self::INCLUDE_DIR . self::normalize($filename);
	}
	static function GetStyle($filename)
	{
		return __URL_ROOT_PATH . self::CONTENT_DIR . 
		       	self::STYLES_DIR . self::normalize($filename);
	}
	static function GetCommon($filename)
	{
		return __ROOT_PATH . self::COMMON_DIR . self::normalize($filename);
	}
	static function GetImage($filename)
	{
		return __URL_ROOT_PATH . self::CONTENT_DIR . 
		       self::IMAGES_DIR . self::normalize($filename);
	}
	static function GetController($controller)
	{
		return __ROOT_PATH . self::CONTROLLERS_DIR . self::normalize($controller) . "Controller.php";
	}
	static function GetView($view, $controller = "")
	{
		return __ROOT_PATH . self::VIEWS_DIR . self::normalize($controller) . self::normalize($view) . ".php";
	}
	static function GetModel($filename)
	{
		return __ROOT_PATH . self::MODELS_DIR . self::normalize($filename) . ".php";
	}
    static function GetModule($filename)
    {
        return __ROOT_PATH . self::MODULES_DIR . self::normalize($filename) . "Module.php";
    }
	static function GetDataMngmnt($filename)
	{
		return __ROOT_PATH . self::DATA_MNGMNT_DIR . self::normalize($filename) . ".php";
	}
	static function GetServerMngmnt($filename)
	{
		return __ROOT_PATH . self::SERVER_MNGMNT_DIR . self::normalize($filename);
	}
}
?>
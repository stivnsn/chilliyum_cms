<?php
include FileStructure::GetModel("UserData");
class AuthenticationProvider
{
    public $user_data;
    private $errorLog;
    function __construct()
    {
        $this->user_data = new UserData();
        $this->processUserSession();
    }

    function processUserSession()
    {
        if(isset($_GET["logout"]))
        {
            unset($_SESSION["user_data"]);
        }
        if (isset($_SESSION["user_data"]) && unserialize($_SESSION["user_data"])->is_logged_in())
        {
            $this->user_data = unserialize($_SESSION["user_data"]);
        }
    }

    static function isAdminLogged(&$user_data)
    {
        return isset($user_data) && $user_data->is_logged_in() && $user_data->is_admin();
    }
    public function getErrorLog()
    {
        return $this->errorLog;
    }

}
?>
<?php
class ServerDataInit
{
	
	public $requestUri;
	public $scriptName;
	public $absServerRoot;
	public $absUrlRoot;
	private $errorLog;
	//public $routeMech;
	function __construct()
	{
		$this->requestUri = $_SERVER["REQUEST_URI"];
		$this->scriptName = $_SERVER["SCRIPT_NAME"];
		$this->absServerRoot = $_SERVER["DOCUMENT_ROOT"];
		$this->absUrlRoot = $_SERVER["HTTP_HOST"];
        	die ($this->requestUri . " " .$this->scriptName . " ". $this->absServerRoot . " " . $this->absUrlRoot);
	}
	public function getErrorLog()
	{
		return $this->errorLog;
	}	
};

class RoutingMechanism
{
    public $controllerFullName;
	public $controllerName;
	public $actionName = "Index";
	public $moduleName;
    public $pseudoModuleName;
    public $params = array();
	public $controllerInstance;
	public $rest = array();
	private $errorLog;
	
	public function __construct()
	{
		if (isset($_GET["module"]))
		{
			$driver = new SettingsDbImpl();
            $this->pseudoModuleName = $_GET["module"];
			if (!$driver->selectModuleByPseudo($this->moduleName, $this->pseudoModuleName))
			{
                echo $driver->getErrorLog();
				$this->moduleName = $this->pseudoModuleName;
			}
		}
		if (isset($_GET["controller"]))
		{
			$this->controllerName = $_GET["controller"];
            $this->controllerFullName = $this->controllerName . "Controller";
		}
		if (isset($_GET["action"]))
		{
			$this->actionName = $_GET["action"];
		}
        if (isset($_GET["params"]))
        {
            $this->params = $_GET["params"];
        }
	}
	public function __deconstruct()
	{
	}
	public function getErrorLog()
	{
		return $this->errorLog;
	}
}


class ShittyRoutingMechanism extends ServerDataInit
{
	public $controller;
	public $action;
	public $rest = array();
	private $routes;
	private $errorLog;
	function __construct()
	{
		ServerDataInit::__construct();
		$uri = substr($this->scriptName, strlen($this->requestUri));
		echo "DON'T USE THIS SHIT!!!";
		if(!$this->route_parse($uri))
		{
			$this->errorLog .= "Invalid uri! \n";
		}
		if (!$this->route_arrange($this->routes))
		{
			$this->errorLog .= "Invalid controller! \n";
		}
		
	}
	function __destruct()
	{
		$this->rest = array();
	}
	function build_path($pathArr, $basePath = '')
	{
		$basePath = trim($basePath, '/');
		foreach ($path as $pathItem)
		{
			$pathArr = explode('/', $pathItem);
			foreach ($pathArr  as $pathArrItem)
			{
				if ($pathArrItem == '' || $pathArrItem == ' ')
				{
					continue;
				}
				$basePath .= '/' . $pathArrItem;
			}
		}	
		return $basePath;
	}
	function route_arrange($route)
	{
		if ($route[0] == NULL)
		{
			$this->errorLog .= "Controller was not properly set. \n";
			return false;
		}
		$this->controller = $route[0];
		if ($route[1] == NULL)
		{
			return true;
		}
		$this->action = $route[1];
		if ($route[2] == NULL)
		{
			return true;
		}
		foreach ($route as $item)
		{
			$this->rest->push($item);
		}
		return true;
	}		
	
	function route_parse($uri)
	{
		$base_uri = $_SERVER["SCRIPT_NAME"];
		$uri = substr($uri, strlen($base_uri));
		if (strstr($uri, '?')) 
		{
			$uri = substr($uri, 0, strpos($uri, '?'));
		}
		$routes = array();
		$routes = explode('/', $uri);
		foreach ($routes as $route)
		{
			if (trim($route) != '')
			{
				array_push($routes, $route);
			}
		}
		if (sizeof($routes) == 0)
		{
			$this->errorLog .= "No routes found. \n";
			return false;
		}
		$this->routes = $routes;
		return true;
	}
	public function getLogErrors()
	{
		return $this->errorLog;
	}
};
?>